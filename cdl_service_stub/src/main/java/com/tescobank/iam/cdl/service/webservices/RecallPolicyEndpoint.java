package com.tescobank.iam.cdl.service.webservices;

import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.tescobank.cdl.service.Credentials;
import com.tescobank.cdl.service.Customer;
import com.tescobank.cdl.service.Policy;
import com.tescobank.cdl.service.PolicyType;
import com.tescobank.cdl.service.RecallPolicyRequest;
import com.tescobank.cdl.service.RecallPolicyResponse;
import com.tescobank.iam.cdl.exception.CustomerReadException;
import com.tescobank.iam.cdl.model.dao.CustomerAndPolicyDAO;
import com.tescobank.iam.cdl.model.dao.vo.CredentialsVO;
import com.tescobank.iam.cdl.model.dao.vo.CustomerVO;
import com.tescobank.iam.cdl.model.dao.vo.PolicyDataVO;

@Endpoint
public class RecallPolicyEndpoint {
	private static final String NAMESPACE_URI = "urn:com:tescobank:cdl:service";


	@Autowired
	private CustomerAndPolicyDAO customerAndPolicyDao;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "RecallPolicyRequest")
	@ResponsePayload
	public RecallPolicyResponse getCustomer(@RequestPayload RecallPolicyRequest request) throws NumberFormatException, CustomerReadException, DatatypeConfigurationException {
		RecallPolicyResponse response = new RecallPolicyResponse();
		CustomerVO customer = customerAndPolicyDao.findCustomeryByPolicyId(Long.valueOf(request.getPolicyNum()));
		Customer responseCustomer = prepareCustomer(customer);
		
		List<Policy> customerPolicies = customer.getCustomerPolicies().stream().map(e -> {
			try {
				return preparePolicy(e);
			} catch (Exception e1) {
				return null;
			}
		}).collect(Collectors.toList());
		responseCustomer.getCustomerPolicies().addAll(customerPolicies);
		

		List<Credentials> customerCredentials = customer.getCustomerCredentials().stream().map(e -> {
			return prepareCredentials(e);
		}).collect(Collectors.toList());
		responseCustomer.getCustomerCredentials().addAll(customerCredentials);
		
		response.setCustomer(responseCustomer);
		return response;
	}
	
	private Customer prepareCustomer(CustomerVO source) throws DatatypeConfigurationException {
		Customer responseCustomer = new Customer();
		
		BeanUtils.copyProperties(source, responseCustomer);
		
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(source.getDateOfBirth());
		responseCustomer.setDateOfBirth(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
		
		responseCustomer.setId(source.getId().toString());
		responseCustomer.setMobile(source.getMobile().toString());
		return responseCustomer;
	}
	
	private Policy preparePolicy(PolicyDataVO policyDataVO) throws DatatypeConfigurationException {
		Policy policy = new Policy();
		policy.setPolicyID(policyDataVO.getId().toString());
		policy.setPolicyType(PolicyType.fromValue(policyDataVO.getPolicyType()));
		
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(policyDataVO.getPolicyStartDate());
		policy.setStartDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));

		return policy;
	}
	
	private Credentials prepareCredentials(CredentialsVO credentials) {
		Credentials responseCredentials = new Credentials();
		BeanUtils.copyProperties(credentials, responseCredentials);
		responseCredentials.setStatus(credentials.isStatus());
		return responseCredentials;
	}
}
