package com.tescobank.iam.gi.origination.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tescobank.iam.cdl.exception.CustomerAndPolicyCreationException;
import com.tescobank.iam.cdl.model.dao.CustomerAndPolicyDAO;
import com.tescobank.iam.cdl.model.dao.vo.CustomerVO;
import com.tescobank.iam.cdl.model.dao.vo.PolicyDataVO;
import com.tescobank.iam.gi.origination.exception.CustomerAndPolicyServiceException;
import com.tescobank.iam.gi.origination.model.GIPolicyData;

@Service
public class CustomerAndPolicyServiceImpl implements CustomerAndPolicyService {
	
	@Autowired
	private CustomerAndPolicyDAO customerAndPolicyDAO;
	

	@Override
	public Long createCustomerAndPolicy(GIPolicyData customerAndPolicyData) throws CustomerAndPolicyServiceException {
		CustomerVO customer = convertPolicyData(customerAndPolicyData);
		Long policyID = 0L;
        try {
        	policyID = customerAndPolicyDAO.createCustomerAndPolicy(customer);
        } catch (CustomerAndPolicyCreationException e) {
            throw new CustomerAndPolicyServiceException("Unable to create customer and policy data " 
            		+ customerAndPolicyData.getFirstName() + " "
            		+ customerAndPolicyData.getSurname() + " "
            		+ customerAndPolicyData.getEmail(), e);
        }
        return policyID;
	}
	
	private CustomerVO convertPolicyData(GIPolicyData customerAndPolicyData) {
		CustomerVO customerVO = new CustomerVO();
		customerVO.setFirstName(customerAndPolicyData.getFirstName());
		customerVO.setSurname(customerAndPolicyData.getSurname());
		customerVO.setDateOfBirth(Date.from(customerAndPolicyData.getDob().atStartOfDay(ZoneId.systemDefault()).toInstant()));
		customerVO.setEmail(customerAndPolicyData.getEmail());
		customerVO.setMobile(Long.valueOf(customerAndPolicyData.getMobile()));
		customerVO.setPostCode(customerAndPolicyData.getPostCode());
		customerVO.setAddressLine1(customerAndPolicyData.getAddressLine1());
		customerVO.setAddressLine2(customerAndPolicyData.getAddressLine2());
		customerVO.setAddressLine3(customerAndPolicyData.getAddressLine3());
		customerVO.setAddressLine4(customerAndPolicyData.getAddressLine4());
		customerVO.setAddressLine5(customerAndPolicyData.getAddressLine5());
		
		PolicyDataVO policyDataVO = new PolicyDataVO();
		policyDataVO.setCustomer(customerVO);
		policyDataVO.setPolicyStartDate(Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant()));
		policyDataVO.setPolicyType(customerAndPolicyData.getPolicyType());
		
		customerVO.addCustomerPolicy(policyDataVO);
		
		return customerVO;
	}

}
