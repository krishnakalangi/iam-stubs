package com.tescobank.iam.gi.origination.exception;

public class CustomerAndPolicyServiceException extends Exception {

	private static final long serialVersionUID = 4937165589374499383L;

	public CustomerAndPolicyServiceException(String message) {
		super(message);
	}

	public CustomerAndPolicyServiceException(Exception cause) {
		super(cause);
	}

	public CustomerAndPolicyServiceException(String message, Exception cause) {
		super(message, cause);
	}

}
