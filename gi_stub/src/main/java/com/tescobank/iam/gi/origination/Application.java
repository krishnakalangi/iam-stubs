package com.tescobank.iam.gi.origination;

import org.hibernate.SessionFactory;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages={"com.tescobank.iam.gi.origination", "com.tescobank.iam.cdl.model.dao"})
@EntityScan(basePackages={"com.tescobank.iam.cdl.model.dao.vo"})
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    @Autowired
    @Bean(name="sessionFactory")
    public SessionFactory sessionFactory(HibernateEntityManagerFactory factory) {
       return factory.getSessionFactory();
    }
}
