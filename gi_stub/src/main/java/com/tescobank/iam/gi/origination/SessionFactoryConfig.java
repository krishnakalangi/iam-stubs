package com.tescobank.iam.gi.origination;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;

@Configuration
public class SessionFactoryConfig {

	// @Bean
	// public LocalSessionFactoryBean annotatedSessionFactory() throws Exception
	// {
	// LocalSessionFactoryBean sessionFactoryBean = new
	// LocalSessionFactoryBean();

	// @SuppressWarnings("rawtypes")
	// Class[] annotatedClasses = new Class[] { CustomerVO.class,
	// PolicyDataVO.class, CredentialsVO.class};
	// sessionFactoryBean.setAnnotatedClasses(annotatedClasses);

	// sessionFactoryBean.setPackagesToScan("com.tescobank.iam.gi.origination.dao.vo");
	//
	// Properties hibernateProperties = new Properties();
	// hibernateProperties.setProperty("hibernate.dialect",
	// "org.hibernate.dialect.Oracle10gDialect");
	// sessionFactoryBean.setHibernateProperties(hibernateProperties);
	//
	// sessionFactoryBean.setDataSource(dataSource());
	//
	// sessionFactoryBean.afterPropertiesSet();
	//
	// return sessionFactoryBean;
	// }

//	@Bean
//	public SessionFactory sessionFactory() {
//
//		Properties hibernateProperties = new Properties();
//		hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
//
//		return new LocalSessionFactoryBuilder(dataSource()).scanPackages("com.tescobank.iam.gi.origination.dao.vo")
//				.addProperties(hibernateProperties).buildSessionFactory();
//	}

//	@Bean
//	public DataSource dataSource() {
//		BasicDataSource dataSource = new BasicDataSource();
//
//		dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
//		dataSource.setUrl("jdbc:oracle:thin:@localhost:1521:XE");
//		dataSource.setUsername("cdldata");
//		dataSource.setPassword("test123");
//
//		return dataSource;
//	}
}
