package com.tescobank.iam.gi.origination.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tescobank.common.logging.annotations.Loggable;
import com.tescobank.iam.gi.origination.exception.CustomerAndPolicyServiceException;
import com.tescobank.iam.gi.origination.model.GIPolicyData;
import com.tescobank.iam.gi.origination.service.CustomerAndPolicyService;

@Controller
@RequestMapping("/gi/origination")
public class OriginationController {
	
    private static final String REDIRECT = "redirect:";
    private static final String FORWARD_SLASH = "/";
	
	@Autowired
	private CustomerAndPolicyService customerAndPolicyService;
	
    @RequestMapping( method = RequestMethod.GET)
    public String startGiOrigination() {
    	return "gi_origination";
    }
    
    @RequestMapping( method = RequestMethod.POST)
    public ModelAndView capturePolicyData(@Valid @Loggable GIPolicyData policyData, RedirectAttributes redirectAttributes) throws CustomerAndPolicyServiceException {
    	Long policyID = customerAndPolicyService.createCustomerAndPolicy(policyData);
    	ModelAndView modelAndView = new ModelAndView();
    	if (policyID != null) {
    		redirectAttributes.addAttribute("email", policyData.getEmail());
    		redirectAttributes.addAttribute("postCode", policyData.getPostCode());
    		redirectAttributes.addAttribute("dob", policyData.getDob());
    		redirectAttributes.addAttribute("policyNumber", policyID);
    		modelAndView.setViewName(getRedirectString() + "web_registration/gi_registration");
    		return modelAndView;
    	}
    	modelAndView.addObject("errorMessage", "Something went wrong while trying to save the customer and policy data. Not enough information to submit data to web registration");
    	modelAndView.setViewName("gi_error");
    	return modelAndView;
    }
    
    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ModelAndView handleError(HttpServletRequest req, Exception exception) {

        ModelAndView mav = new ModelAndView("gi_error");

        mav.addObject("errorMessage", exception.getMessage());
        return mav;
    }
    
    private String getRedirectString(){

        return REDIRECT + FORWARD_SLASH;
    }

}
