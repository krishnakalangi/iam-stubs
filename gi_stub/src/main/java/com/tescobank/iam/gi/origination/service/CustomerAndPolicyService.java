package com.tescobank.iam.gi.origination.service;

import com.tescobank.iam.gi.origination.exception.CustomerAndPolicyServiceException;
import com.tescobank.iam.gi.origination.model.GIPolicyData;

public interface CustomerAndPolicyService {
	Long createCustomerAndPolicy(GIPolicyData cutomerAndPolicyData) throws CustomerAndPolicyServiceException;
}
