package com.tescobank.iam.cdl.model.dao;

import com.tescobank.iam.cdl.exception.CustomerAndPolicyCreationException;
import com.tescobank.iam.cdl.exception.CustomerReadException;
import com.tescobank.iam.cdl.exception.CustomerUpdationException;
import com.tescobank.iam.cdl.model.dao.vo.CustomerVO;

public interface CustomerAndPolicyDAO {
	
	Long createCustomerAndPolicy(CustomerVO customerVO) throws CustomerAndPolicyCreationException;
	
	CustomerVO findCustomeryByPolicyId(Long policyID) throws CustomerReadException;
	
	Long updateCustomerAndGetCredentialsToken(CustomerVO customerVO) throws CustomerUpdationException;

}
