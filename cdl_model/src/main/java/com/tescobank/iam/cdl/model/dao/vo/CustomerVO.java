package com.tescobank.iam.cdl.model.dao.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CUSTOMER")
public class CustomerVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String firstName;
	private String surname;
	private Date dateOfBirth;
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;
	private String addressLine4;
	private String addressLine5;
	private String postCode;
	private Long mobile;
	private String email;
	private Set<PolicyDataVO> customerPolicies = new HashSet<>();
	private Set<CredentialsVO> customerCredentials = new HashSet<>();
	
	@Id
	@SequenceGenerator(name="CUSTOMER_SEQ", sequenceName="CUSTOMER_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUSTOMER_SEQ")
	@Column(name="ID")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="FIRST_NAME")
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Column(name="SURNAME")
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	@Column(name="DOB")
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	@Column(name="ADDRESS_LINE1")
	public String getAddressLine1() {
		return addressLine1;
	}
	
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	
	@Column(name="ADDRESS_LINE2")
	public String getAddressLine2() {
		return addressLine2;
	}
	
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	
	@Column(name="ADDRESS_LINE3")
	public String getAddressLine3() {
		return addressLine3;
	}
	
	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}
	
	@Column(name="ADDRESS_LINE4")
	public String getAddressLine4() {
		return addressLine4;
	}
	
	public void setAddressLine4(String addressLine4) {
		this.addressLine4 = addressLine4;
	}
	
	@Column(name="ADDRESS_LINE5")
	public String getAddressLine5() {
		return addressLine5;
	}
	
	public void setAddressLine5(String addressLine5) {
		this.addressLine5 = addressLine5;
	}
	
	@Column(name="POSTCODE")
	public String getPostCode() {
		return postCode;
	}
	
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	
	@Column(name="MOBILE")
	public Long getMobile() {
		return mobile;
	}
	
	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}
	
	@Column(name="EMAIL")
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	@OneToMany(mappedBy = "customer", cascade=CascadeType.ALL)
	public Set<PolicyDataVO> getCustomerPolicies() {
		return customerPolicies;
	}

	public void setCustomerPolicies(Set<PolicyDataVO> customerPolicies) {
		this.customerPolicies = customerPolicies;
	}
	
	public void addCustomerPolicy(PolicyDataVO customerPolicy) {
		if (customerPolicies == null) {
			customerPolicies = new HashSet<>();
		}
		customerPolicies.add(customerPolicy);
	}

	@OneToMany(mappedBy = "customer", cascade=CascadeType.ALL)
	public Set<CredentialsVO> getCustomerCredentials() {
		return customerCredentials;
	}

	public void setCustomerCredentials(Set<CredentialsVO> customerCredentials) {
		this.customerCredentials = customerCredentials;
	}
	
	public void addCustomerCredentails(CredentialsVO credentials) {
		if (customerCredentials == null) {
			customerCredentials = new HashSet<>();
		}
		customerCredentials.add(credentials);
	}
	
}
