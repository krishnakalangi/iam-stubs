package com.tescobank.iam.cdl.exception;

public class CustomerAndPolicyCreationException extends Exception {

	private static final long serialVersionUID = 4937165589374499383L;

	public CustomerAndPolicyCreationException(String message) {
		super(message);
	}

	public CustomerAndPolicyCreationException(Exception cause) {
		super(cause);
	}

	public CustomerAndPolicyCreationException(String message, Exception cause) {
		super(message, cause);
	}

}