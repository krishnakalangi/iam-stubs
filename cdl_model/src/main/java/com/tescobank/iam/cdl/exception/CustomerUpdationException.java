package com.tescobank.iam.cdl.exception;

public class CustomerUpdationException extends Exception {

	private static final long serialVersionUID = 4937165589374499383L;

	public CustomerUpdationException(String message) {
		super(message);
	}

	public CustomerUpdationException(Exception cause) {
		super(cause);
	}

	public CustomerUpdationException(String message, Exception cause) {
		super(message, cause);
	}

}