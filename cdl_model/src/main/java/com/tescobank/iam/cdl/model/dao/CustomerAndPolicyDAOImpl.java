package com.tescobank.iam.cdl.model.dao;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tescobank.iam.cdl.exception.CustomerAndPolicyCreationException;
import com.tescobank.iam.cdl.exception.CustomerReadException;
import com.tescobank.iam.cdl.exception.CustomerUpdationException;
import com.tescobank.iam.cdl.model.dao.vo.CustomerVO;
import com.tescobank.iam.cdl.model.dao.vo.PolicyDataVO;

@Repository
@Transactional
public class CustomerAndPolicyDAOImpl implements CustomerAndPolicyDAO {
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public CustomerAndPolicyDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public Long createCustomerAndPolicy(CustomerVO customerVO) throws CustomerAndPolicyCreationException {
        try {
            getSession().save(customerVO);
            return customerVO.getCustomerPolicies().iterator().next().getId();
        } catch (Exception e) {
            throw new CustomerAndPolicyCreationException("Error saving CustomerVO: ", e);
        }
	}
	
	@Override
	public Long updateCustomerAndGetCredentialsToken(CustomerVO customerVO) throws CustomerUpdationException {
        try {
            getSession().saveOrUpdate(customerVO);
            return customerVO.getCustomerCredentials().iterator().next().getId();
        } catch (Exception e) {
            throw new CustomerUpdationException("Error updating CustomerVO: ", e);
        }
	}

	@Override
	public CustomerVO findCustomeryByPolicyId(Long policyID) throws CustomerReadException {
        try {
        	CustomerVO customer =  PolicyDataVO.class.cast(getSession().load(PolicyDataVO.class, policyID)).getCustomer();
            customer.getCustomerPolicies().size();
            customer.getCustomerCredentials().size();
            return customer;
        } catch (Exception e) {
            throw new CustomerReadException("Error Reading CustomerVO: ", e);
        }
	}

}
