package com.tescobank.iam.cdl.exception;

public class CustomerReadException extends Exception {

	private static final long serialVersionUID = 4937165589374499383L;

	public CustomerReadException(String message) {
		super(message);
	}

	public CustomerReadException(Exception cause) {
		super(cause);
	}

	public CustomerReadException(String message, Exception cause) {
		super(message, cause);
	}

}