package com.tescobank.iam.cdl.model.dao.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="POLICYDATA")
public class PolicyDataVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String policyType;
	private Date policyStartDate;
	
	private CustomerVO customer;
	
	@Id
	@Column(name="ID")
	@SequenceGenerator(name="POLICYDATA_SEQ", sequenceName="POLICYDATA_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "POLICYDATA_SEQ")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="POLICY_TYPE")
	public String getPolicyType() {
		return policyType;
	}
	
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}
	
	@Column(name="POLICY_START_DATE")
	public Date getPolicyStartDate() {
		return policyStartDate;
	}
	
	public void setPolicyStartDate(Date policyStartDate) {
		this.policyStartDate = policyStartDate;
	}

	@ManyToOne
	@JoinColumn(name="CUSTOMER_ID", nullable=false)
	public CustomerVO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerVO customer) {
		this.customer = customer;
	}
}
