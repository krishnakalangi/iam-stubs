set echo ON;
set termout ON;
/**
* GI - CDL data store for IAM POC
*/

set termout OFF;
set feedback ON;
set pages 50000;
set linesize 1000;
set serveroutput on;

set termout ON;
/* Executing portal_drop_users_factory_cascade_ora.sql (Step 1 of 5) */
/* This Script drops Users, Roles and Objects */
@@cdl_drop_users_cascade_ora.sql
set termout OFF;

set termout ON;
/* Executing portal_create_schema_ora.sql (Step 2 of 5) */
/* This Script creates users, roles and assignments  */
@@cdl_create_schema_ora.sql
set termout OFF;

set termout ON;
/* Executing portal_create_objects_*_ora.sql (Step 3 of 5) */
/* This Script creates tables, indexes, and sequences  */
@@cdl_create_objects.sql
set termout OFF;

quit;
