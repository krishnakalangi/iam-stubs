-- GI - CDL data store for IAM POC

-- RUN THIS SCRIPT AS SYS USER
-- THIS SPECIFIC SCRIPT ONLY TO BE RUN ON AN INITIAL SCHEMA SETUP IN DEV.
 
------------------------------
-- CREATE APPLICATION USERS --
------------------------------

CREATE USER CDLDATA
	IDENTIFIED BY "test123"
	DEFAULT TABLESPACE USERS
	QUOTA UNLIMITED ON USERS
	TEMPORARY TABLESPACE TEMP;

---------------------------------------------
-- ROLE CREATE SESSION RIGHTS
---------------------------------------------
GRANT CREATE SESSION TO CDLDATA;

